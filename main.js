// Теоретичні питання
// 1.Примітивні типи:
// числа (number)
// рядки (string)
// булеві значення (boolean)
// null
// undefined
// символи 

// Об'єктні типи:
// масиви (array)
// об'єкти (object)
// функції (function)
// дата (date)
// регулярні вирази (regexp)

// 2.Різниця між операторами == і === полягає у їхньому способі порівняння значень.
//     Оператор == порівнює значення зі слабкою типізацією, тобто він може перетворювати типи даних, якщо це необхідно, щоб порівняти їх.
//     Наприклад, 2 == '2' буде true, оскільки оператор == автоматично перетворює рядок '2' на число 2, щоб порівняти їх.

//     Оператор === порівнює значення зі строгим типізацією, тобто він не перетворює типи даних, а порівнює їх напряму.
//         Наприклад, 2 === '2' буде false, оскільки тип даних(число і рядок) не співпадає.

// 3. Оператор - це символ або знак, який вказує JavaScript на виконання певної операції, такої як додавання, віднімання, множення або порівняння. 
// Наприклад, оператор + використовується для додавання двох чисел, а оператор == використовується для порівняння двох значень.


function getUserInfo() {
let name = prompt("Please enter your name:");
let age = prompt("Please enter your age:");

  // перевіряємо чи коректно введені дані
    while (!name || !age || isNaN(age)) {
    name = prompt("Please enter your name:");
    age = prompt("Please enter your age:");
    }

    return { name, age };
}

function checkAge() {
const { name, age } = getUserInfo();

    if (age < 18) {
    alert ("You are not allowed to visit this website.");
    } else if (age >= 18 && age <= 22) {
    const result = confirm("Are you sure you want to continue?");
        if (result) {
    alert(`Welcome, ${name}`);
    } else {
    alert("You are not allowed to visit this website.");
    }
} else {
    alert(`Welcome, ${name}`);
}
}

checkAge();


